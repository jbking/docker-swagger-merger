FROM node:alpine

RUN npm install -g swagger-merger

ENTRYPOINT ["swagger-merger"]
